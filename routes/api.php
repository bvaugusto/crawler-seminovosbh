<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => '/v1'], function () {

    //Veiculo
    Route::get('/veiculo/cidade', 'Api\VeiculoController@getCidade');
    Route::get('/veiculo/cidade-veiculo/{tipoVeiculo}/{idMarca}/{idModelo}', 'Api\VeiculoController@getCidadePorVeiculoMarcaModelo');
    Route::get('/veiculo/opcoes-veiculo-tipo', 'Api\VeiculoController@getOpcoesVeiculoTipo');
    Route::get('/veiculo/marca/{tipo}', 'Api\VeiculoController@getMarca');
    Route::get('/veiculo/modelo/{codMarca}', 'Api\VeiculoController@getModeloPorMarca');
    Route::apiResource('/veiculo', 'Api\VeiculoController');
});
