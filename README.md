Crawler SeminovosBH
================================
Api - https://www.seminovosbh.com.br

Instalação
------------

    composer install
    
Instalação
------------    
    
Inicialização 
    
    cp .env.example .env
    
    composer install && composer update && php artisan key:generate
    
    php artisan serve
    
URL padrão de acesso:  http://127.0.0.1:8000
    
## Veículo

* GET /api/v1/veiculo/cidade

> http://127.0.0.1:8000/api/v1/veiculo/cidade

```
{
    idCidade int
    cidade string
    dDD int
    DDD int
    idEstado int
    sigla: string
}
```

* GET /api/v1/veiculo/cidade-veiculo/{tipoVeiculo}/{idMarca}/{idModelo}

> http://127.0.0.1:8000/api/v1/veiculo/cidade-veiculo/3/95/2309

```
{
    tipoVeiculo string
    idMarca string
    idModelo string
}
```

* GET /api/v1/veiculo/opcoes-veiculo-tipo

> http://127.0.0.1:8000/api/v1/veiculo/opcoes-veiculo-tipo

```
{
    Object {
            id int
            nome string
            marcas array
        }
}
```

* GET /api/v1/veiculo/marca/{tipo}

> http://127.0.0.1:8000/api/v1/veiculo/marca/3
```
{
    idMarca int
    marca string
}
```

* GET /api/v1/veiculo/modelo/{codMarca}

> http://127.0.0.1:8000/api/v1/veiculo/modelo/95
```
{
    idModelo int
    modelo string
}
```

* GET /api/v1/veiculo/{codVeiculo}

> http://127.0.0.1:8000/api/v1/veiculo/2547622
```
{
    big_image_veiculo string 
    titulo_veiculo string
    valor_veiculo string
    detalhes_veiculo object
    observacoes_veiculo object
    contato_veiculo object
}
```

* POST /api/v1/veiculo

> http://127.0.0.1:8000/api/v1/veiculo
```
{
    veiculo:3
    marca:95
    modelo: 2309
    idCidade:2700
    valor1:
    valor2:
    ano1:
    ano2:
    particular:
    revenda:
}
```
```
{
    veiculo int
    marca int
    modelo int
    idCidade int
    valor1 decimal
    valor2 decimal
    ano1 date
    ano2 date
    particular int
    revenda int
}
```



