<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\GuzzleRequester;
use App\Http\Requests\StoreVeiculoFormRequest;
use GuzzleHttp\Client;
use Goutte\Client as ClientGoutte;
use Illuminate\Http\Request;

class VeiculoController extends Controller
{
    const urlSeminovosBH = 'https://seminovosbh.com.br';
    const urlSeminovos = 'https://seminovos.com.br';

    protected $arrayTipoVeiculo = [1 => 'carro', 2 => 'caminhao', 3 => 'moto'];

    /**
     * VeiculoController constructor.
     */
    public function __construct()
    {
        $this->guzzleClient = new GuzzleRequester();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * @param array $arrayMarca
     * @param string $idMarca
     * @return mixed|string
     */
    private function getMarcaPorId($arrayMarca, $idMarca)
    {
        $arrayMarca = json_decode($arrayMarca, true);

        foreach ($arrayMarca as $item) {
            if ($item['idMarca'] == $idMarca) {
                return $item['marca'];
            }
        }
    }

    private function mountUrlStore($storeVeiculoFormRequest)
    {
        $url = '';

        $veiculo = $this->arrayTipoVeiculo[$storeVeiculoFormRequest['veiculo']];
        if (!empty($veiculo)) {
            $url .= '/veiculo/'.$veiculo;
        }

        $arrayMarca = $this->getMarca($storeVeiculoFormRequest['veiculo']);
        $marca = $this->getMarcaPorId($arrayMarca, $storeVeiculoFormRequest['marca']);
        if (!empty($marca)) {
            $url .= '/marca/'.$marca;
        }

        $modelo = $storeVeiculoFormRequest['modelo'];
        if (!empty($modelo)) {
            $url .= '/modelo/'.$modelo;
        }

        $cidade = $storeVeiculoFormRequest['idCidade'];
        if (!empty($cidade)) {
            $url .= '/cidade/'.$cidade;
        }

        $valor1 = $storeVeiculoFormRequest['valor1'];
        if (!empty($cidade)) {
            $url .= '/valor1/'.$valor1;
        }

        $valor2 = $storeVeiculoFormRequest['valor2'];
        if (!empty($valor2)) {
            $url .= '/valor2/'.$valor2;
        }

        $ano1 = $storeVeiculoFormRequest['ano1'];
        if (!empty($ano1)) {
            $url .= '/ano1/'.$ano1;
        }

        $ano2 = $storeVeiculoFormRequest['ano2'];
        if (!empty($ano2)) {
            $url .= '/ano2/'.$ano2;
        }

        $particular = $storeVeiculoFormRequest['particular'];
        if (!empty($particular)) {
            $url .= '/particular/'.$particular;
        }

        $revenda = $storeVeiculoFormRequest['revenda'];
        if (!empty($revenda)) {
            $url .= '/revenda/'.$revenda;
        }

        return self::urlSeminovosBH.'/resultadobusca/index'.$url;
    }

    /**
     * @param StoreVeiculoFormRequest $storeVeiculoFormRequest
     * @return false|string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function store(StoreVeiculoFormRequest $storeVeiculoFormRequest)
    {
        $clientGoutte = new ClientGoutte();
        $crawler = $clientGoutte->request('GET', self::mountUrlStore($storeVeiculoFormRequest->all()));

        $result = array();
        $crawler->filter('#topoConteudoBusca')->each(function($index) use (&$result) {

            $count = $index->filter('.bg-busca')->count();
            for ($i = 0; $i < $count; $i++) {
                $link_veiculo = $index->filter('.bg-busca .titulo-busca a')->eq($i)->link()->getUri();
                $link_veiculo = explode("/", $link_veiculo);
                $result[] = [
                    'url_veiculo' => '/api/v1/veiculo/'.$link_veiculo[7],
                    'titulo_veiculo' => $index->filter('.bg-busca .titulo-busca h4')->eq($i)->eq(0)->text(),
                    'preco_veiculo' => $index->filter('.bg-busca .titulo-busca h4 span.preco_busca')->eq($i)->text(),
                    'image_veiculo' => $index->filter('.bg-busca img')->eq($i)->attr("src"),
                    'ano_veiculo' => $index->filter('.bg-busca .bg-nitro-mais-home')->eq($i)->filter('dd p')->eq(0)->text(),
                    'km_veiculo' => $index->filter('.bg-busca .bg-nitro-mais-home')->eq($i)->filter('dd p')->eq(1)->text(),
                    'cilindradas_veiculo' => $index->filter('.bg-busca .bg-nitro-mais-home')->eq($i)->filter('dd p')->eq(2)->text(),
                    'cor_veiculo' => $index->filter('.bg-busca .bg-nitro-mais-home')->eq($i)->filter('dd p')->eq(3)->text(),
                    'combustivel_veiculo' => $index->filter('.bg-busca .bg-nitro-mais-home')->eq($i)->filter('dd p')->eq(4)->text(),
                    'condicao_veiculo' => $index->filter('.bg-busca .bg-nitro-mais-home')->eq($i)->filter('dd span')->eq(0)->text()
                ];
            }
        });

        return json_encode($result, JSON_FORCE_OBJECT);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response|false|string
     */
    public function show($id)
    {
        $url = self::urlSeminovosBH.'/veiculo/codigo/'.$id;
        $result = array();

        $clientGoutte = new ClientGoutte();
        $crawler = $clientGoutte->request('GET', $url);

        $result['big_image_veiculo'] = $crawler->filter('#bigImage')->attr("src");
        $result['titulo_veiculo'] = $crawler->filter('#textoBoxVeiculo > h5')->text();
        $result['valor_veiculo'] = $crawler->filter('#textoBoxVeiculo > p')->text();

        $crawler->filter('#infDetalhes')->each(function($index) use (&$result) {
            $count = $index->filter('ul > li')->count();
            for ($i = 0; $i < $count; $i++) {
                $item[] = $index->filter('ul li')->eq($i)->text();
            }
            $result['detalhes_veiculo'] = $item;
        });

        $crawler->filter('#infDetalhes2')->each(function($index) use (&$result) {
            $count = $index->filter('ul > li')->count();
            for ($i = 0; $i < $count; $i++) {
                $item[] = $index->filter('ul li')->eq($i)->text();
            }
            $result['acessorios_veiculo'] = $item;
        });

        $crawler->filter('#infDetalhes3')->each(function($index) use (&$result) {
            $count = $index->filter('ul > p')->count();
            for ($i = 0; $i < $count; $i++) {
                $item[] = $index->filter('ul p')->eq($i)->text();
            }
            $result['observacoes_veiculo'] = $item;
        });

        $crawler->filter('#infDetalhes4')->each(function($index) use (&$result) {
            $count = $index->filter('ul > li')->count();
            for ($i = 0; $i < $count; $i++) {
                $item[] = $index->filter('ul li')->eq($i)->text();
            }
            $result['contato_veiculo'] = $item;
        });

        return json_encode($result, JSON_FORCE_OBJECT);
    }

    /**
     * @param string $tipoVeiculo
     * @param string $idMarca
     * @param string $idModelo
     * @return string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getCidadePorVeiculoMarcaModelo($tipoVeiculo, $idMarca, $idModelo)
    {
        $response = $this->guzzleClient->send( 'GET', self::urlSeminovosBH.'/json/index/busca-cidades/veiculo/'.$tipoVeiculo.'/marca/'.$idMarca.'/modelo/'.$idModelo.'/cidade/0/data.js', []);
        return (string) $response->getBody();
    }

    /**
     * @return string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getCidade()
    {
        $response = $this->guzzleClient->send( 'GET', self::urlSeminovos.'/cidades', []);
        return (string) $response->getBody();
    }

    /**
     * @return string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getOpcoesVeiculoTipo()
    {
        $response = $this->guzzleClient->send( 'GET', self::urlSeminovos.'/filtros', []);
        return (string) $response->getBody();
    }

    /**
     * @param string $tipo
     * @return array|string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getMarca($tipo)
    {
        if (!array_key_exists($tipo, $this->arrayTipoVeiculo)) {
            return response()->json(['success' => false, 'message' => 'Tipo do veículo inválido!']);
        }

        $response = $this->guzzleClient->send( 'GET', self::urlSeminovosBH.'/marcas/buscamarca/tipo/'.$tipo, []);
        return (string) $response->getBody();
    }

    public function getModeloPorMarca($codMarca)
    {
        $response = $this->guzzleClient->send('GET', self::urlSeminovosBH.'/json/modelos/buscamodelo/marca/'.$codMarca.'/data.js', []);
        return (string) $response->getBody();
    }
}
