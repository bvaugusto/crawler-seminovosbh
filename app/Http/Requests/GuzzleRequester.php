<?php

namespace App\Http\Requests;

use GuzzleHttp\Client;

class GuzzleRequester implements APIRequester {

    /**
     * @param string $method
     * @param string $endpoint
     * @param array $options
     * @return mixed|\Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function send($method, $endpoint, $options)
    {
        $client = new Client();
        return $client->request($method, $endpoint, $options);
    }
}