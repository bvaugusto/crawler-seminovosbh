<?php

namespace App\Http\Requests;

interface APIRequester {

    /**
     * @param string $method
     * @param string $endpoint
     * @param array $options
     * @return mixed
     */
    public function send($method, $endpoint, $options);
}